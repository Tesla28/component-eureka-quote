FROM openjdk:11
ADD target/docker-component-eureka.jar docker-component-eureka.jar
EXPOSE 8761
ENTRYPOINT ["java", "-jar", "docker-component-eureka.jar"]